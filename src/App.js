import './App.css';
import {useState} from "react";
import OrderDetailsContainer from "./components/OrderDetailsContainer/OrderDetailsContainer";
import FoodItemContainer from "./components/FoodItemContainer/FoodItemContainer";
import drinkImage from './assets/images/cup.png';
import mealImage from './assets/images/knife-fork.png';

const FOOD_ITEMS = [
    {id: 1, name: 'Hamburger', price: 80, count: 0, sum: 0, display: false, imgSrc: mealImage},
    {id: 2, name: 'Coffee', price: 70, count: 0, sum: 0, display: false, imgSrc: drinkImage},
    {id: 3, name: 'Cheeseburger', price: 90, count: 0, sum: 0, display: false, imgSrc: mealImage},
    {id: 4, name: 'Tea', price: 50, count: 0, sum: 0, display: false, imgSrc: drinkImage},
    {id: 5, name: 'Fries', price: 45, count: 0, sum: 0, display: false, imgSrc: mealImage},
    {id: 6, name: 'Cola', price: 40, count: 0, sum: 0, display: false, imgSrc: drinkImage}
];

const App = () => {
    const [foodItems, setFoodItems] = useState(FOOD_ITEMS);
    const [orderTotalPrice, setOrderTotalPrice] =useState(0);

    const addFoodItem = (foodItemId) => {
        setFoodItems(foodItems.map(item => {
            if (item.id === foodItemId) {
                item.count++;
                item.sum = item.price * item.count;
                item.display = true;
            }
            return item;
        }));
        getOrderTotalPrice();
    };

    const deleteFoodItem = (foodItemId) => {
        setFoodItems(foodItems.map(item => {
            if (item.id === foodItemId) {
                item.count = 0;
                item.sum = 0;
                item.display = false;
            }
            return item;
        }));
        getOrderTotalPrice();
    };

    const getOrderTotalPrice = () => {
        setOrderTotalPrice(
            foodItems.reduce((acc, item) => {
                acc += item.sum;
                return acc;
            }, 0)
        );
    };

    return (
        <div className="App">
            <OrderDetailsContainer
                foodItems={foodItems}
                orderTotalPrice={orderTotalPrice}
                deleteFoodItem={deleteFoodItem}
            />
            <FoodItemContainer
                foodItems={foodItems}
                addFoodItem={addFoodItem}
            />
        </div>
    );
}

export default App;
