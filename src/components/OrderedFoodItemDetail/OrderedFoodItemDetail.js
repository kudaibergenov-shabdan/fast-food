import React from 'react';
import './OrderedFoodItemDetail.css';
import OrderedFoodItem from "../OrderedFoodItem/OrderedFoodItem";

const OrderedFoodItemDetail = props => {
    let initialInfoClasses = [];
    let totalPriceClasses = ['total-price'];
    if (props.orderTotalPrice === 0) {
        totalPriceClasses.push('hide');
    } else {
        initialInfoClasses.push('hide');
    }
    const foodItemComponent = props.foodItems.map(item => (
            <OrderedFoodItem
                key={item.id}
                item={item}
                deleteFoodItem={() => props.deleteFoodItem(item.id)}
            />
    ));

    return (
        <div>
            <p className={initialInfoClasses.join(' ')}>
                <span className="empty-order">Order id Empty!</span>
                <span className="empty-order">Please add some items!</span>
            </p>
            {foodItemComponent}
            <p className={totalPriceClasses.join(' ')}>Total price = {props.orderTotalPrice}</p>
        </div>
    );
};

export default OrderedFoodItemDetail;