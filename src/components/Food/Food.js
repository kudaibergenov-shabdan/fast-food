import React from 'react';
import './Food.css';

const Food = props => {
    return (
        <div className="btn-block">
            <button className="btn-food" onClick={props.addFoodItem}>
                <div className="btn-food-inner">
                    <div className="food-image-block">
                        <img src={props.foodItem.imgSrc} alt={props.foodItem.name} className="food-image"/>
                    </div>
                    <div className="food-block">
                        <h4>{props.foodItem.name}</h4>
                        <p>Price: {props.foodItem.price} KGS</p>
                    </div>
                </div>
            </button>

        </div>
    );
};

export default Food;