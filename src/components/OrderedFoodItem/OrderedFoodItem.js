import React from 'react';
import './OrderedFoodItem.css';

const OrderedFoodItem = props => {
    let foodItemClasses = ['food-items-block'];
    if (!props.item.display) {
        foodItemClasses.push('hide');
    }
    return (
        <div className={foodItemClasses.join(' ')}>
            <span className="food-item-info food-item-info-title">{props.item.name}</span>
            <span className="food-item-info">x{props.item.count}</span>
            <span className="food-item-info">{props.item.sum}</span>
            <button className="food-item-info-delete-btn"
                    onClick={props.deleteFoodItem}>X</button>
        </div>
    );
};

export default OrderedFoodItem;