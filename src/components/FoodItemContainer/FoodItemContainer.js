import React from 'react';
import Food from "../Food/Food";

const FoodItemContainer = props => {
    const foodItemsComponent = props.foodItems.map(foodItem => (
        <Food key={foodItem.id} foodItem={foodItem} addFoodItem={() => {props.addFoodItem(foodItem.id)}}/>
    ));

    return (
        <fieldset className="box">
            <legend>FoodItem</legend>
            {foodItemsComponent}
        </fieldset>
    );
};

export default FoodItemContainer;