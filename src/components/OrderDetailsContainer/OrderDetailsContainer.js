import React from 'react';
import OrderedFoodItemDetail from "../OrderedFoodItemDetail/OrderedFoodItemDetail";

const OrderDetailsContainer = props => {
    return (
        <fieldset className="box">
            <legend>OrderDetails</legend>
            <OrderedFoodItemDetail
                foodItems={props.foodItems}
                orderTotalPrice={props.orderTotalPrice}
                deleteFoodItem={props.deleteFoodItem}/>
        </fieldset>
    );
};

export default OrderDetailsContainer;